class  Pos									//TOTAL: 24 bytes
{
	float m_x;										//4 bytes
	float m_y;										//4 bytes
	float m_z;										//4 bytes
public:

	const float &x = m_x;							//4 bytes
	const float &y = m_y;							//4 bytes
	const float &z = m_z;							//4 bytes

	constexpr Pos() : m_x(0), m_y(0), m_z(0) {};

	constexpr inline Pos(float a, float b, float c) : m_x(a), m_y(b), m_z(c) {};

	constexpr inline void Update(Pos p)
	{
		m_x = p.m_x;
		m_y = p.m_y;
		m_z = p.m_z;
	}

	constexpr inline void Update(float nx, float ny, float nz)
	{
		m_x = nx;
		m_y = ny;
		m_z = nz;
	}

	constexpr inline Pos& operator=( Pos &p)
	{
		m_x = p.x;
		m_y = p.y;
		m_z = p.z;
		return *this;
	}

	constexpr inline Pos operator+( Vector2 &vec)
	{
		return Pos(m_x + vec.x, m_y + vec.y, m_z);
	}

	constexpr inline Pos &operator+=( Vector2 &vec)
	{
		m_x += vec.x;
		m_y += vec.y;
		return *this;
	}
	constexpr inline Pos operator-( Vector2 &vec)
	{
		return Pos(m_x - vec.x, m_y - vec.y, m_z);
	}

	constexpr inline Pos &operator-=( Vector2 &vec)
	{
		m_x -= vec.x;
		m_y -= vec.y;
		return *this;
	}

	inline Vector2 operator-( Pos p)
	{
		return Vector2(m_x, m_y, p.x, p.y);
	}
};