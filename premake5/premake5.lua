local vcpkg = {}
vcpkg["path"] = "PATH" -- <-- Change this (e.g. D:/dev/vcpkg)
vcpkg["targetOS"] = "OS" -- <-- Change this  (e.g.  x86-windows)
workspace ("APPNAME")
	configurations ({ "Debug", "Release"})
project ("APPNAME")
	kind ("ConsoleApp")
	language ("C++")
	cppdialect ("C++17")	
	targetdir ("bin/%{cfg.buildcfg}")
	files ({ "**.cpp", "**.hpp" })

	includedirs({ vcpkg["path"].."/installed/".. vcpkg["targetOS"] .."/include", "include"})
	
	filter ("configurations:Debug")
		libdirs ({	vcpkg["path"].."/installed/".. vcpkg["targetOS"] .."/debug/lib"})
	
		defines ({"DEBUG"})
		symbols ("On")
		
	filter ("configurations:Release")

		libdirs ({	vcpkg["path"].."/installed/".. vcpkg["targetOS"] .."/lib"})
	
		defines ({"NDEBUG"})
		optimize ("On")